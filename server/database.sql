CREATE DATABASE perntodo;

CREATE TABLE todo(
  todo_id SERIAL PRIMARY KEY,
  description VARCHAR(255)
);

CREATE TABLE news_user(
  id_news_user SERIAL PRIMARY KEY,
  title VARCHAR(255),
  text_news text,
  image text,
  id_user VARCHAR(255)
);