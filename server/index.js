const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db");

// for file
const fileUpload = require("express-fileupload");
//const morgan = require("morgan");

//middleware
app.use(cors());
app.use(express.json()); //req.body


//
app.use(fileUpload({
  createParentPath: true
}))

//
app.use(express.urlencoded({ extended: true }))


//INSERT INTO news_user (title, text_news, image, id_user) VALUES('t4tsehuhsmgrt', 't4tenjseijsjrt', DEFAULT, 1)




app.post("/picture", async(req,res)=>{
  try {
     if(!req.files){
      res.send({
         status: false,
         message: "No files"
       })
     } else {
      const {picture} = req.files;
      picture.mv("./uploads" + picture.name); 
      res.send({
        status: true,
        message: "File is uploaded!"
      })
     }  
  } catch (err) {
      console.error(err.message)
  }
  });




//Create a todo
app.post("/news", async(req,res)=>{
  //await
  try {
    
      const {Title,TextOfNews,Filename} = req.body;

      const {Filename} = req.files;
      Filename.mv("./uploads" + Filename.name); 
 
      const newTodo = await pool.query(
          "INSERT INTO news_user (title,text_news, image, id_user) VALUES($1, $2, $3, 1) RETURNING *",
          [Title,TextOfNews,Filename]
      );
       console.log(req.body);
      // console.log(title)
      // console.log(text_news)  
      // console.log(text_news);
  
       console.log(newTodo.rows[0]);

      // res.json(newTodo.rows[0]);
  } catch (err) {
      console.error(err.message)
  }
  });


app.listen(5000, () => {
  console.log("server has started on port 5000");
});

