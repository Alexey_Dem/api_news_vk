import React, { Fragment, useState } from "react";
import { useForm } from 'react-hook-form';

const InputNews = () => {

    const [Title, SetTitle] = useState("Прибраться");//стандартное состояние
    const [TextOfNews, SetTextOfNews] = useState("Прибраться");
    const fileInput  = React.createRef(); //принимает файл
    const { register, handleSubmit } = useForm();
    
   // const [idNews, SetidNews] = useState("Прибраться");

    const onSubmitForm = async (e) => {

        e.preventDefault();
        try {
            const Filename= fileInput.current.files[0].name ;
            const body = { Title, TextOfNews, Filename };

            const response = await fetch("http://localhost:5000/news", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body)
            });

/*
            const formData = new FormData();
            formData.append("picture", data.picture[0])
            
            const response1 = await fetch("http://localhost:5000/picture", {
                method: "POST",
                body: formData
            });*/

        //    window.location="/"; //для обновления страницe
            // console.log(body);
             console.log(response);
        } catch (err) {
            console.error(err.message)

        }
    };

    /*const onSubmit = async (data) => {
        const formData = new FormData();
        formData.append("picture", data.picture[0])
        
        const response1 = await fetch("http://localhost:5000/picture", {
            method: "POST",
            body: formData
        })

    }
    
    ///   <form onSubmit={handleSubmit(onSubmit)}>
                   <input ref={register} type="file" name="picture"></input>
                   <button>Save</button>
            </form>
    
    */

    return (
        <Fragment>
            <h1 className="text-center mt-5">Добавить новость</h1>
            <br />
            <form className="mb-3" onSubmit={onSubmitForm}>
                <label>
                Введите вашу тему для новости:
                <input type="text" className="form-control" value={Title} onChange={e => SetTitle(e.target.value)}></input>
                </label>
                <br />
                <label>
                Введите описание новости: 
                <input type="text" className="form-control" value={TextOfNews} onChange={e => SetTextOfNews(e.target.value)}></input>
                </label>
                <br />
                <label>
                Загрузите картинку: 
                <br />           
                
            <input type="file" ref={fileInput}       className="form-file-input" ></input>      
                </label>
              <br />
                <button className="btn btn-success">Опубликовать</button>

            </form>
        </Fragment>
    );
};
export default InputNews;