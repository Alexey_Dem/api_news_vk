import React, { Fragment } from "react";
import "./App.css";

//components

import InputNews from "./components/InputNews";

function App() {
  return (
    <Fragment>
      <div className="container">
        <InputNews />
      </div>
    </Fragment>
  );
}

export default App;
